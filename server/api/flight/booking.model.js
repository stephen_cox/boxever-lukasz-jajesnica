'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var BookingSchema = new Schema({
  firstName: String,
  lastName: String,
  address: String,
  email: String
});

module.exports = mongoose.model('Booking', BookingSchema);
