'use strict';

var express = require('express');
var controller = require('./flight.controller');
var config = require('../../config/environment');

var router = express.Router();

// search result generator route
router.get('/', controller.index);

// booking route
router.post('/book', controller.book);

module.exports = router;
