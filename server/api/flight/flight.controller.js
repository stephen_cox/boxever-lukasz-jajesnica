'use strict';

var config = require('../../config/environment');
var faker = require('faker');
var _ = require('lodash');
var moment = require('moment');
var Booking = require('./booking.model');

exports.book = function (req, res) {
  Booking.create(req.body, function(err) {
    if(err) { return handleError(res, err); }
    return res.status(201).json({status: 'success', message: 'Booking created'});
  });
};

exports.index = function (req, res) {
  var destination = req.query.destination.split(',');
  if (!destination.length === 2) {
    res.status(500).json({
      error: 'Destination is not specified'
    });
  }

  var origin = req.query.origin.split(',');
  if (!origin.length === 2) {
    res.status(500).json({
      error: 'Origin country is not specified'
    });
  }

  var airLines = ['Air France', 'Aer Lingus', 'Ryanair', 'Quantas', 'Finnair'];

  res.status(200).json({
    flights: _.times(_.random(3, 20), function (n) {
      return {
        date: moment().format('MM-DD-YYYY'),
        origin: {
          country: origin[1],
          city: origin[0],
          time: moment().format('HH:mm')
        },
        destination: {
          country: destination[1],
          city: destination[0],
          time: moment().add({hours: _.random(1, 30)}).format('HH:mm')
        },
        flightData: {
          id: (n + 1),
          flightNumber: faker.fake('{{random.number}}'),
          airline: airLines[_.random(0, 4)],
          price: _.random(100, 1000),
          currencySymbol: '$'
        }
      }
    })
  });
};
