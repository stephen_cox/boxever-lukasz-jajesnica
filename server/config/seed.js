/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var Booking = require('../api/flight/booking.model');

// clearing old bookings
Booking.find({}).remove(function() {

});
