'use strict';

angular.module('boxeverApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl'
      })
      .state('bookFlight', {
        url: '/bookFlight/{flightId}',
        templateUrl: 'app/main/book.html',
        controller: 'BookCtrl'
      });
  });
