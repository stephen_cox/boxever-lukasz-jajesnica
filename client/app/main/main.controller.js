'use strict';

angular.module('boxeverApp')
  .controller('MainCtrl', function ($scope, $http, flightsData) {
    $scope.flights = [];
    $scope.searchMode = false;
    $scope.origin = "Dublin, Ireland";

    $scope.search = function () {
      $scope.message = null;
      if ($scope.destination && $scope.destination.indexOf(',') && $scope.destination.split(',').length == 2) {
        $scope.searchMode = true;
        $http.get('/api/flights?origin=' + $scope.origin + 'd&destination=' + $scope.destination).success(function (flights) {
          $scope.flights = flights.flights;
          flightsData.flightsData = $scope.flights;
        });
      } else {
        alert('Please enter flight destination in format CITY, COUNTRY');
      }
    }
  })
  .controller('BookCtrl', function ($scope, $http, $stateParams, flightsData) {
    var flightData = flightsData.flightsData[$stateParams.flightId];
    $scope.flight = flightData;
    $scope.book = function () {
      $scope.message = {};
      console.log($scope.firstName, $scope.lastName, $scope.address, $scope.email)
      if($scope.firstName && $scope.lastName && $scope.address && $scope.email) {
        $http.post('/api/flights/book',
          {
            firstName: $scope.firstName,
            lastName: $scope.lastName,
            address: $scope.address,
            email: $scope.email
          }).success(function () {
            $scope.message = {
              status: 'success',
              message: 'Your flight is booked. Enjoy.'
            };
            if ($('#bookingForm')) {
              $('#bookingForm')[0].reset();
            }
          }).error(function () {
            $scope.message = {
              status: 'error',
              message: 'There was unexpected error during placing your booking. Please try again.'
            }
          });
      } else {
        $scope.message = {
          status: 'error',
          message: 'All informations are required.'
        }
      }
    }
  })
  .factory('flightsData', function () {
    return {
      flightsData: []
    }
  });
